package de.wernertoex.supporttx.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.data.Mitarbeiter;
import de.wernertoex.supporttx.repositories.KundeRepository;
import de.wernertoex.supporttx.repositories.MitarbeiterRepository;
import de.wernertoex.supporttx.util.MailerBean;
import de.wernertoex.supporttx.util.Events.Added;
import de.wernertoex.supporttx.util.Events.Deleted;
import de.wernertoex.supporttx.util.Events.Updated;
import de.wernertoex.supporttx.util.LoggerQual.FachLog;

@SessionScoped
@Named
public class KundeController implements Serializable {

	private static final long serialVersionUID = -4209172851479439592L;

	@Inject
	@Added
	private Event<Kunde> kundeAddEvent;

	@Inject
	@Updated
	private Event<Kunde> kundeUpdateEvent;

	@Inject
	@Deleted
	private Event<Mitarbeiter> mitarbeiterDeleteEvent;
	
	@Inject
	private KundeRepository kundeRepository;

	@Inject
	private MitarbeiterRepository mitarbeiterRepository;

	@Inject
	private KundeUebersichtController kundeUebersichtController;

	@Inject
	private MitarbeiterController mitarbeiterController;

	@Inject
	private EmailController emailController;
	
	@Inject
	@FachLog
	private Logger logger;

	private Kunde kunde;
	private ControllerModes mode;
	private List<Mitarbeiter> mitarbeiters;

	private ControllerModes getMode() {
		return mode;
	}

	void loadMitarbeiter() {
		mitarbeiters = mitarbeiterRepository.findByKunde(kunde);
	}

	public void setKundeToEdit(ControllerModes mode) {
		setKundeToEdit(mode, new Kunde());
	}

	public void setKundeToEdit(ControllerModes mode, Kunde kunde) {
		this.kunde = kunde;
		this.mode = mode;
		loadMitarbeiter();
	}

	public String doSave() {
		if (getMode() == ControllerModes.ADD) {
			kundeRepository.persist(kunde);
			kundeAddEvent.fire(kunde);
		}
		if (getMode() == ControllerModes.EDIT) {
			kunde = kundeRepository.saveEntity(kunde);
			kundeUpdateEvent.fire(kunde);
		}
		kundeUebersichtController.init();
		return Pages.KUNDE_UEBERSICHT;
	}

	public String doCancel() {
		return Pages.KUNDE_UEBERSICHT;
	}

	public String getTitle() {
		return getMode() == ControllerModes.EDIT ? "Kunde editieren"
				: "Neuen Kunden anlegen";
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}

	public String doAddMitarbeiter() {
		System.out.println("Add Aktion");
		mitarbeiterController.setMitarbeiterToEdit(ControllerModes.ADD, kunde);
		return Pages.MITARBEITER_EDIT;
	}

	public String doEditMitarbeiter(Mitarbeiter mitarbeiter) {
		mitarbeiterController.setMitarbeiterToEdit(ControllerModes.EDIT,
				mitarbeiter);
		return Pages.MITARBEITER_EDIT;
	}

	public void doDeleteMitarbeiter(Mitarbeiter mitarbeiter) {
		mitarbeiterRepository.delete(mitarbeiter);
		mitarbeiterDeleteEvent.fire(mitarbeiter);
		
	}

	public List<Mitarbeiter> getMitarbeiters() {
		return mitarbeiters;
	}

	public void setMitarbeiters(List<Mitarbeiter> mitarbeiters) {
		this.mitarbeiters = mitarbeiters;
	}

	public void onMitarbeiterAdded(@Observes @Added Mitarbeiter mitarbeiter) {
		logger.info("Mitarbeiter hinzugefügt");
		mitarbeiters.add(mitarbeiter);
	}

	public void onMitarbeiterDeleted(@Observes @Deleted Mitarbeiter mitarbeiter) {
		mitarbeiters.remove(mitarbeiter);
	}

	public void onMitarbeiterUpdated(@Observes @Updated Mitarbeiter mitarbeiter) {
		logger.info("Mitarbeiter aktualisiert");
		mitarbeiters.remove(mitarbeiter);
		mitarbeiters.add(mitarbeiter);
	}

	public boolean isEditable(){
		return this.mode == ControllerModes.ADD || this.mode == ControllerModes.EDIT;
	}
	
	public String doMitarbeiterMail(Mitarbeiter mitarbeiter){
		if (!StringUtils.isBlank(mitarbeiter.getEmail())){
			emailController.setToMailAdress(mitarbeiter.getEmail());
			return Pages.EMAIL;
		}
		return "";
	}
	
}
