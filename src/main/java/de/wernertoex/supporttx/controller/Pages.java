package de.wernertoex.supporttx.controller;

public class Pages {

	public static final String KUNDE_UEBERSICHT = "Kunden.xhtml"+"?faces-redirect=true";
	public static final String KUNDE_EDIT = "KundeEdit.xhtml"+"?faces-redirect=true";
	public static final String MITARBEITER_EDIT = "Mitarbeiter.xhtml"+"?faces-redirect=true";
	public static final String EMAIL = "EMail.xhtml"+"?faces-redirect=true";
}
