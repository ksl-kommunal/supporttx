package de.wernertoex.supporttx.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;

import de.wernertoex.supporttx.application.MyApplication;
import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.repositories.KundeRepository;
import de.wernertoex.supporttx.util.Events.Added;
import de.wernertoex.supporttx.util.Events.Deleted;
import de.wernertoex.supporttx.util.Events.Updated;
import de.wernertoex.supporttx.util.LoggerQual.FachLog;

@Named
@SessionScoped
public class KundeUebersichtController implements Serializable {

	private static final long serialVersionUID = -5872397484519155109L;

	private List<Kunde> kunden;
	private List<Kunde> filteredKunden;
	
	private Kunde selectedKunde;
	
	@Inject @FachLog
	Logger logger;
	
	@Inject
	@Deleted
	private Event<Kunde> kundeDeleteEvent;

	@Inject
	private MyApplication myApplication;

	@Inject
	private KundeRepository kundenRepository;

	@Inject
	private KundeController kundeController;
	
	public List<Kunde> getKunden() {		
		return kunden;
	}

	@PostConstruct
	void init(){
		myApplication.init(); 
		kunden= kundenRepository.findAll();			
	}

	public String doAddKunde(){
		System.out.println("Add Aktion");
		kundeController.setKundeToEdit(ControllerModes.ADD);
		return Pages.KUNDE_EDIT;
	}
	
	public String doEditKunde(Kunde kunde){
		kundeController.setKundeToEdit(ControllerModes.EDIT, kunde);
		return Pages.KUNDE_EDIT;
	}
	
	public String doShowKunde(Kunde kunde){
		logger.info("doShowKunde");
		kundeController.setKundeToEdit(ControllerModes.SHOW, kunde);
		return Pages.KUNDE_EDIT;
	}
	
	public void doDeleteKunde(Kunde kunde){
		kundenRepository.delete(kunde);
		kundeDeleteEvent.fire(kunde);
	}
	
	public Kunde getSelectedKunde() {
		return selectedKunde;
	}

	public void setSelectedKunde(Kunde selectedKunde) {
		this.selectedKunde = selectedKunde;
	}

	public List<Kunde> getFilteredKunden() {
		return filteredKunden;
	}

	public void setFilteredKunden(List<Kunde> filteredKunden) {
		this.filteredKunden = filteredKunden;
	}
	
	public void onKundeAddEvent(@Observes @Added Kunde kunde){
		this.kunden.add(kunde);
		logger.info("Kunde hinzugefügt");
	}
	
	public void onKundeUpdateEvent(@Observes @Updated Kunde kunde){
		int index = kunden.indexOf(kunde);
		if (index >=0)
		this.kunden.set(index,kunde);
	}
	
	public void onKundeDeleteEvent(@Observes @Deleted Kunde kunde){
		kunden.remove(kunde);
	}
	
}
