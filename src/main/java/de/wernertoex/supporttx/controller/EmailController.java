package de.wernertoex.supporttx.controller;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.wernertoex.supporttx.util.MailerBean;

@Named
@RequestScoped
public class EmailController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2220497968519386863L;

	@Inject
	private MailerBean mailer;
	
	private String mailBody;
	private String mailSubject;
	private String toMailAdress;
	
	
	
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getToMailAdress() {
		return toMailAdress;
	}
	public void setToMailAdress(String toMailAdress) {
		this.toMailAdress = toMailAdress;
	}
	
	public String sendMail(){
		mailer.setMailBody(mailBody);
		mailer.setMailSubject(mailSubject);
		mailer.setToAdress(toMailAdress);
		mailer.sendMail();
		return "";
	}
	
	
	
}
