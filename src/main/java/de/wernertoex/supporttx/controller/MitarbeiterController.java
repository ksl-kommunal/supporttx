package de.wernertoex.supporttx.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.data.Mitarbeiter;
import de.wernertoex.supporttx.repositories.MitarbeiterRepository;
import de.wernertoex.supporttx.util.Events.Added;
import de.wernertoex.supporttx.util.Events.Updated;

@SessionScoped
@Named
public class MitarbeiterController implements Serializable {

	private static final long serialVersionUID = -4209172851479439592L;

	@Inject
	@Added
	private Event<Mitarbeiter> mitarbeiterAddEventSrc;

	@Inject
	@Updated
	private Event<Mitarbeiter> mitarbeiterUpdateEventSrc;

	@Inject
	private MitarbeiterRepository repository;

	private Mitarbeiter mitarbeiter;
	private ControllerModes mode;

	private ControllerModes getMode() {
		return mode;
	}

	public void setMitarbeiterToEdit(ControllerModes mode, Kunde kunde) {
		Mitarbeiter newMitarbeiter = new Mitarbeiter();
		newMitarbeiter.setKunde(kunde);
		setMitarbeiterToEdit(mode, newMitarbeiter);
	}

	public void setMitarbeiterToEdit(ControllerModes mode,
			Mitarbeiter mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
		this.mode = mode;
	}

	public String doSave() {
		if (getMode() == ControllerModes.ADD) {
			repository.persist(mitarbeiter);
			mitarbeiterAddEventSrc.fire(mitarbeiter); 
		}
		if (getMode() == ControllerModes.EDIT) {
			mitarbeiter = repository.saveEntity(mitarbeiter);
			mitarbeiterUpdateEventSrc.fire(mitarbeiter);
		}
		return Pages.KUNDE_EDIT;
	}

	public String doCancel() {
		return Pages.KUNDE_EDIT;
	}

	public String getTitle() {
		return getMode() == ControllerModes.EDIT ? "Kunde editieren"
				: "Neuen Kunden anlegen";
	}

	public Mitarbeiter getMitarbeiter() {
		return mitarbeiter;
	}

	public void setMitarbeiter(Mitarbeiter mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
	}

}
