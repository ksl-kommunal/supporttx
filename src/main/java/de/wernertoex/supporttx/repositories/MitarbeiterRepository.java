package de.wernertoex.supporttx.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.data.Mitarbeiter;

@Stateless
@Named
public class MitarbeiterRepository extends AbstractRepository<Mitarbeiter> {

	
	private static final long serialVersionUID = -68608438317889251L;

	
	
	@PersistenceContext
	protected void setEntityManager(EntityManager em) {
		this.em = em;
		
	}

	public List<Mitarbeiter> findAll(){
	 return	em.createNamedQuery(Mitarbeiter.findAll).getResultList();

	}

	@Override
	protected Class<Mitarbeiter> getEntityClass() {
		return Mitarbeiter.class;
	}

	public List<Mitarbeiter> findByKunde(Kunde kunde) {
	
		List<Mitarbeiter> retList;
		
		try{
			retList = em.createNamedQuery(Mitarbeiter.findByKunde).setParameter("kunde", kunde).getResultList();
		} catch (NoResultException nre) {
			retList = new ArrayList<Mitarbeiter>();
		} catch (Exception e){
			retList = new ArrayList<Mitarbeiter>();
		}
		return retList;
	}

}
