package de.wernertoex.supporttx.repositories;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import de.wernertoex.supporttx.data.AbstractEntity;

/**
 * 
 * Stellt ein Repository zur Verwaltung von JPA - Entities zur Verfügung.
 * 
 * @author wernertoex
 * 
 * @param <E>
 *            Typ der Entity, die verwaltet werden soll.
 */
@Stateless
public abstract class AbstractRepository<E extends AbstractEntity> implements
		Serializable {

	private static final long serialVersionUID = 1803073264811003623L;

	protected EntityManager em;

	protected abstract void setEntityManager(EntityManager em);

	public E saveEntity(E entity) {
		if (entity.isEntityTransient()) {
			em.persist(entity);
			return entity;
		} else
			return em.merge(entity);
	}

	public void persist(E entity) {
		em.persist(entity);
	}

	public E merge(E entity) {
		return em.merge(entity);
	}

	protected abstract Class<E> getEntityClass();

	public E findById(Integer id) {
		return em.find(getEntityClass(), id);
	}

	public void delete(E entity) {
		E managed = findById(entity.getId());
		em.remove(managed);
	}

}
