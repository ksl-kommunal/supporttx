package de.wernertoex.supporttx.repositories;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.util.Events.Added;
import de.wernertoex.supporttx.util.LoggerQual.FachLog;

@Stateless
@Named
public class KundeRepository extends AbstractRepository<Kunde> {

	@Inject
	@Added
	private Event<Kunde> addedSource;
	
	@Inject
	@FachLog
	private Logger logger;
	
	private static final long serialVersionUID = -68608438317889251L;

	
	
	@PersistenceContext
	protected void setEntityManager(EntityManager em) {
		this.em = em;
		
	}

	public List<Kunde> findAll(){
	 return	em.createNamedQuery(Kunde.findAll).getResultList();

	}

	@Override
	protected Class<Kunde> getEntityClass() {
		return Kunde.class;
	}
	
}
