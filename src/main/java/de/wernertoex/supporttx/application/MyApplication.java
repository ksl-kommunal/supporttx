package de.wernertoex.supporttx.application;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.data.Mitarbeiter;
import de.wernertoex.supporttx.repositories.KundeRepository;
import de.wernertoex.supporttx.repositories.MitarbeiterRepository;
import de.wernertoex.supporttx.util.LoggerQual.TecLog;

@Named
@ApplicationScoped
public class MyApplication implements Serializable {

	private static final long serialVersionUID = -2339581344703751469L;
	@Inject
	private KundeRepository kundeRepository;

	@Inject
	private MitarbeiterRepository mitarbeiterRepository;

	@Inject
	@TecLog
	private Logger logger;

	@PostConstruct
	public void init(){
		logger.info("Init");
		List<Kunde> kunden = kundeRepository.findAll();
	try{
		if (null == kunden || kunden.size() == 0 ){
			Kunde kd1 = new Kunde();
			kd1.setAnschrift("MusterStraße 10");
			kd1.setEmail("mustermann@verwaltung.de");
			kd1.setKdName("Stadt Musterstadt");
			kd1.setOrt("Musterstadt");
			kd1.setPlz("99096");
			kd1.setTelefon("0123 456789");
			
			Kunde kd2 = new Kunde();
			kd2.setAnschrift("Erfurter Straße 10");
			kd2.setEmail("mustermann@Ostfestfalen.de");
			kd2.setKdName("Stadt in Ostwestfalen");
			kd2.setOrt("Ostwestfalen");
			kd2.setPlz("99096");
			kd2.setTelefon("0123 987654");
			kundeRepository.saveEntity(kd1);
			kundeRepository.saveEntity(kd2);
			
			
			 kd1 = new Kunde();
			kd1.setAnschrift("MusterStraße 10");
			kd1.setEmail("mustermann@verwaltung.de");
			kd1.setKdName("Stadt Musterstadt");
			kd1.setOrt("Osterhausen");
			kd1.setPlz("99096");
			kd1.setTelefon("0123 456789");
			kundeRepository.saveEntity(kd1);
			
			Mitarbeiter m1 = new Mitarbeiter();
			m1.setKunde(kd1);
			m1.setVorname("Peter");
			m1.setZuname("Gedöns");
			m1.setEmail("peter@gedoens.de");
			mitarbeiterRepository.saveEntity(m1);
			
			m1 = new Mitarbeiter();
			m1.setKunde(kd1);
			m1.setVorname("Paul");
			m1.setZuname("Schnickenfittich");
			m1.setEmail("paul@schnickenfittich.de");
			mitarbeiterRepository.saveEntity(m1);
			
			
			
			kd2 = new Kunde();
			kd2.setAnschrift("Erfurter Straße 10");
			kd2.setEmail("mustermann@Ostfestfalen.de");
			kd2.setKdName("Stadt in Ostwestfalen");
			kd2.setOrt("Münsterland");
			kd2.setPlz("99096");
			kd2.setTelefon("0123 987654");
		
			kundeRepository.saveEntity(kd2);
			

			 kd1 = new Kunde();
			kd1.setAnschrift("MusterStraße 10");
			kd1.setEmail("mustermann@verwaltung.de");
			kd1.setKdName("Osterhasencity");
			kd1.setOrt("Osterhausen am Inn");
			kd1.setPlz("99096");
			kd1.setTelefon("0123 456789");
			
			kd2 = new Kunde();
			kd2.setAnschrift("Erfurter Straße 10");
			kd2.setEmail("mustermann@Ostfestfalen.de");
			kd2.setKdName("Stadt in Ostwestfalen");
			kd2.setOrt("Schieferhausen");
			kd2.setPlz("99096");
			kd2.setTelefon("0123 987654");
			kundeRepository.saveEntity(kd1);
			kundeRepository.saveEntity(kd2);
			
			

			 kd1 = new Kunde();
			kd1.setAnschrift("MusterStraße 10");
			kd1.setEmail("mustermann@verwaltung.de");
			kd1.setKdName("Stadt Musterstadt");
			kd1.setOrt("Weihnachtsstadt");
			kd1.setPlz("99096");
			kd1.setTelefon("0123 456789");
			
			kd2 = new Kunde();
			kd2.setAnschrift("Erfurter Straße 10");
			kd2.setEmail("mustermann@Ostfestfalen.de");
			kd2.setKdName("Stadt in Ostwestfalen");
			kd2.setOrt("Vögelbar");
			kd2.setPlz("99096");
			kd2.setTelefon("0123 987654");
			kundeRepository.saveEntity(kd1);
			kundeRepository.saveEntity(kd2);
			
		} 
			
		}catch (RuntimeException re){
			
		}
		
	}

}
