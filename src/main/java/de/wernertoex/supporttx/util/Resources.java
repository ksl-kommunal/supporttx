package de.wernertoex.supporttx.util;


import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import de.wernertoex.supporttx.util.LoggerQual.FachLog;
import de.wernertoex.supporttx.util.LoggerQual.TecLog;



public class Resources {

	
	@Produces 
	@FachLog 
	public Logger produceLog(InjectionPoint injectionPoint) {
		return  Logger.getLogger("FachLog: " + injectionPoint.getMember().getDeclaringClass()
				.getName());
	}

	@Produces
	@TecLog
	public Logger produceTecLog(InjectionPoint injectionPoint) {
		return  Logger.getLogger("TecLog: " + injectionPoint.getMember().getDeclaringClass()
				.getName());
	}
	
	@Produces
	@RequestScoped
	public FacesContext produceFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	@Produces
	@RequestScoped
	public HttpServletRequest produseRquest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}
	
//	@Produces
//	@PersistenceContext
//	private EntityManager em;
}
