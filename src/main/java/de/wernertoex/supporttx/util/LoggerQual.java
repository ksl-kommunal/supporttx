package de.wernertoex.supporttx.util;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

public class LoggerQual {

	@Qualifier
	@Target({TYPE, METHOD, FIELD, PARAMETER})
	@Retention(RUNTIME)
	public @interface FachLog{}
	
	@Qualifier
	@Target({TYPE, METHOD, FIELD, PARAMETER})
	@Retention(RUNTIME)
	public @interface TecLog{}
}
