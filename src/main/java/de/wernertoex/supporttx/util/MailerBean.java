package de.wernertoex.supporttx.util;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import de.wernertoex.supporttx.util.LoggerQual.TecLog;

@SessionScoped
public class MailerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2723681094867331593L;

	@Inject
	@TecLog
	Logger logger;

	private HtmlEmail email = new HtmlEmail();

	private String toAdress;
	private String fromAdress;
	private String mailSubject;
	private String mailBody;
	private String smtpServer;
	private String smtpHost;
	private String smtpPwd;

	public String getToAdress() {
		return toAdress;
	}

	public void setToAdress(String toAdress) {
		this.toAdress = toAdress;
	}

	public String getFromAdress() {
		return fromAdress;
	}

	public void setFromAdress(String fromAdress) {
		this.fromAdress = fromAdress;
	}

	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public void sendMail() {
		// Create the email message
		try {
			email.setHostName("smtp.1und1.de");
			email.addTo(toAdress);
			email.setFrom("webmaster@ksl-kommunal.de", "Me");
			email.setAuthentication("webmaster@ksl-kommunal.de", "*maLi81#wjT75/");
			email.setSSL(true);
			email.setSmtpPort(587);
			email.setSubject(mailSubject);

			// set the html message
			email.setHtmlMsg(mailBody);

			// set the alternative message
			email.setTextMsg("Your email client does not support HTML messages");

			// send the email
			email.send();
		} catch (EmailException ee) {
			logger.severe("Mail fehlgeschlagen: " + ee.toString());
		}
	}

}
