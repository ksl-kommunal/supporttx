package de.wernertoex.supporttx.data;

import javax.persistence.*;

@Entity
public class IssueAttachment extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String attachment;

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String param) {
		this.attachment = param;
	}
}