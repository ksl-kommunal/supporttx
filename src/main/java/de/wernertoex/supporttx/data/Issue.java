package de.wernertoex.supporttx.data;

import javax.persistence.*;

import java.util.List;

import de.wernertoex.supporttx.data.IssueAttachment;
import de.wernertoex.supporttx.data.IssueComment;
import de.wernertoex.supporttx.data.Kunde;
import de.wernertoex.supporttx.data.Mitarbeiter;
import de.wernertoex.supporttx.data.Modul;

@Entity
public class Issue extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String titel;
	private String beschreibung;

	@ManyToOne
	private Kunde kunde;

	@ManyToOne
	private Modul modul;

	@ManyToOne
	private Mitarbeiter bearbeiter;
	private String status;
	@OneToMany
	private List<IssueAttachment> issueAttachment;
	@OneToMany
	private List<IssueComment> issueComment;

	public Issue() {
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String param) {
		this.titel = param;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String param) {
		this.beschreibung = param;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde param) {
		this.kunde = param;
	}

	public Modul getModul() {
		return modul;
	}

	public void setModul(Modul param) {
		this.modul = param;
	}

	public Mitarbeiter getBearbeiter() {
		return bearbeiter;
	}

	public void setBearbeiter(Mitarbeiter param) {
		this.bearbeiter = param;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String param) {
		this.status = param;
	}

	public List<IssueAttachment> getIssueAttachment() {
	    return issueAttachment;
	}

	public void setIssueAttachment(List<IssueAttachment> param) {
	    this.issueAttachment = param;
	}

	public List<IssueComment> getIssueComment() {
	    return issueComment;
	}

	public void setIssueComment(List<IssueComment> param) {
	    this.issueComment = param;
	}
}