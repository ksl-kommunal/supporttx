package de.wernertoex.supporttx.data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Modul")
public class Modul extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String bezeichnung;
	public Modul() {
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String param) {
		this.bezeichnung = param;
	}
}