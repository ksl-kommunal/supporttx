package de.wernertoex.supporttx.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name="Kunde.findAll", query="select k from Kunde k")
})
public class Kunde extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	public static final String findAll = "Kunde.findAll";
	
	private String kdName;
	private String anschrift;
	private String plz;
	private String ort;
	private String email;
	private String telefon;
	private String telefax;
	
	@OneToMany(mappedBy = "kunde")
	private List<Mitarbeiter> mitarbeiters;
	@OneToMany
	private List<Vertrag> vertraege;
	public String getKdName() {
		return kdName;
	}

	public void setKdName(String param) {
		this.kdName = param;
	}

	public String getAnschrift() {
		return anschrift;
	}

	public void setAnschrift(String anschrift) {
		this.anschrift = anschrift;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public List<Mitarbeiter> getMitarbeiters() {
	    return mitarbeiters;
	}

	public void setMitarbeiters(List<Mitarbeiter> param) {
	    this.mitarbeiters = param;
	}

	public List<Vertrag> getVertraege() {
	    return vertraege;
	}

	public void setVertraege(List<Vertrag> param) {
	    this.vertraege = param;
	}

	public String getTelefax() {
		return telefax;
	}

	public void setTelefax(String telefax) {
		this.telefax = telefax;
	}

	
}