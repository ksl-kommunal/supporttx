package de.wernertoex.supporttx.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Seminar extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String bezeichnung;
	private String datum;
	private String ort;
	private String beschreibung;
	@OneToMany(mappedBy = "seminar")
	private List<Teilnehmer> teilnehmer;
	private Integer maxTeilnehmer;
	@ManyToOne
	private Mitarbeiter seminarLeiter;
	public Seminar() {
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String param) {
		this.bezeichnung = param;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String param) {
		this.datum = param;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String param) {
		this.ort = param;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String param) {
		this.beschreibung = param;
	}

	public List<Teilnehmer> getTeilnehmer() {
		return teilnehmer;
	}

	public void setTeilnehmer(List<Teilnehmer> param) {
		this.teilnehmer = param;
	}

	public Integer getMaxTeilnehmer() {
		return maxTeilnehmer;
	}

	public void setMaxTeilnehmer(Integer maxTeilnehmer) {
		this.maxTeilnehmer = maxTeilnehmer;
	}

	public Mitarbeiter getSeminarLeiter() {
	    return seminarLeiter;
	}

	public void setSeminarLeiter(Mitarbeiter param) {
	    this.seminarLeiter = param;
	}
}