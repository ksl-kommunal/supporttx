package de.wernertoex.supporttx.data;

import javax.persistence.*;

import de.wernertoex.supporttx.data.Mitarbeiter;
import de.wernertoex.supporttx.data.Seminar;

@Entity
public class Teilnehmer extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String andmeldedat;

	@ManyToOne
	private Seminar seminar;
	
	@ManyToOne
	private Mitarbeiter mitarbeiter;

	public String getAndmeldedat() {
		return andmeldedat;
	}

	public void setAndmeldedat(String param) {
		this.andmeldedat = param;
	}

	public Seminar getSeminar() {
		return seminar;
	}

	public void setSeminar(Seminar param) {
		this.seminar = param;
	}

	public Mitarbeiter getMitarbeiter() {
		return mitarbeiter;
	}

	public void setMitarbeiter(Mitarbeiter param) {
		this.mitarbeiter = param;
	}

}