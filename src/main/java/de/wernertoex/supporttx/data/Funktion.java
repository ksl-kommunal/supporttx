package de.wernertoex.supporttx.data;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Funktion extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String bezeichnung;
	@ManyToMany(mappedBy = "funktion")
	private List<Mitarbeiter> mitarbeiter;

	public Funktion() {
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String param) {
		this.bezeichnung = param;
	}

	public Collection<Mitarbeiter> getMitarbeiter() {
	    return mitarbeiter;
	}

	public void setMitarbeiter(List<Mitarbeiter> param) {
	    this.mitarbeiter = param;
	}

}