package de.wernertoex.supporttx.data;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name="Mitarbeiter.findAll", query="Select m FROM Mitarbeiter m"),
	@NamedQuery(name="Mitarbeiter.findByKunde", query="Select m FROM Mitarbeiter m WHERE m.kunde=:kunde")
})
public class Mitarbeiter extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	public static final String findAll = "Mitarbeiter.findAll";
	public static final String findByKunde = "Mitarbeiter.findByKunde";
	
	@ManyToOne
	private Kunde kunde;
	private String vorname;
	private String zuname;
	private String telefon;
	private String telefax;
	private String mobil;
	private String email;
	private String loginName;
	private String pwhash;
	private String pwdsalt;
	private Boolean isonuser;

	@ManyToMany
	private List<Funktion> funktion;

	

	@OneToMany(mappedBy = "mitarbeiter")
	private List<Teilnehmer> teilnehmer;

	@OneToMany(mappedBy = "seminarLeiter")
	private List<Seminar> seminarAlsLeitung;

	@OneToMany
	private List<Logins> logins;

	public Mitarbeiter() {
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde param) {
		this.kunde = param;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String param) {
		this.vorname = param;
	}

	public String getZuname() {
		return zuname;
	}

	public void setZuname(String param) {
		this.zuname = param;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String param) {
		this.telefon = param;
	}

	public String getMobil() {
		return mobil;
	}

	public void setMobil(String param) {
		this.mobil = param;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String param) {
		this.email = param;
	}

	public Collection<Funktion> getFunktion() {
		return funktion;
	}

	public void setFunktion(List<Funktion> param) {
		this.funktion = param;
	}

	public String getPwhash() {
		return pwhash;
	}

	public void setPwhash(String param) {
		this.pwhash = param;
	}

	public String getPwdsalt() {
		return pwdsalt;
	}

	public void setPwdsalt(String param) {
		this.pwdsalt = param;
	}

	public Boolean getIsonuser() {
		return isonuser;
	}

	public void setIsonuser(Boolean param) {
		this.isonuser = param;
	}

	public List<Teilnehmer> getTeilnehmer() {
		return teilnehmer;
	}

	public void setTeilnehmer(List<Teilnehmer> param) {
		this.teilnehmer = param;
	}

	public List<Seminar> getSeminarAlsLeitung() {
		return seminarAlsLeitung;
	}

	public void setSeminarAlsLeitung(List<Seminar> param) {
		this.seminarAlsLeitung = param;
	}

	public List<Logins> getLogins() {
	    return logins;
	}

	public void setLogins(List<Logins> param) {
	    this.logins = param;
	}

	public String getTelefax() {
		return telefax;
	}

	public void setTelefax(String telefax) {
		this.telefax = telefax;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

}