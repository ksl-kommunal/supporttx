package de.wernertoex.supporttx.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * 
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	private static final long serialVersionUID = -8403919523078478338L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	protected Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		throw new UnsupportedOperationException();
		// Not supported
	}

	@Version
	// Version wird hochgezählt.für Optimistic Locking
	@Column(name = "version")
	private int version = 0;

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Temporal(TemporalType.TIMESTAMP)
	private Date createTimeStamp;

	@Temporal(TemporalType.TIMESTAMP)
	private Date changeTimeStamp;

	private Integer createUserId;

	private Integer changeUserId;

	public Date getCreateTimeStamp() {
		return createTimeStamp;
	}

	protected void setCreateTimeStamp(Date createTimeStamp) {
		this.createTimeStamp = createTimeStamp;
	}

	public Date getChangeTimeStamp() {
		return changeTimeStamp;
	}

	public void setChangeTimeStamp(Date changeTimeStamp) {
		this.changeTimeStamp = changeTimeStamp;
	}

	@PrePersist
	// Vor Anlage, kein Update
	@Column(nullable = false, updatable = false)
	public void prePersist() {
		Date date = new Date();
		setCreateTimeStamp(date);
		setChangeTimeStamp(date);
		setChangeUserId(1);
		setCreateUserId(1);
	}

	@PreUpdate
	public void preUpdate() {
		setChangeTimeStamp(new Date());
		setChangeUserId(1);
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (getId() != null)
			result += "id: " + getId();
		return result;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		if (getId() != null) {
			return getId().equals(((AbstractEntity) that).getId());
		}
		return super.equals(that);
	}

	@Override
	public int hashCode() {
		if (getId() != null) {
			return getId().hashCode();
		}
		return super.hashCode();
	}

	public boolean isEntityTransient() {
		return getId() == null;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer param) {
		this.createUserId = param;
	}

	public Integer getChangeUserId() {
		return changeUserId;
	}

	public void setChangeUserId(Integer param) {
		this.changeUserId = param;
	}
}