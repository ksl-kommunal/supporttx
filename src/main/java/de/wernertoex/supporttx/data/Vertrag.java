package de.wernertoex.supporttx.data;

import javax.persistence.Entity;

import de.wernertoex.supporttx.data.Modul;

import javax.persistence.ManyToOne;

@Entity
public class Vertrag extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String asbschlussDatum;
	private String kuendigung;
	private String bemerkung;
	
	@ManyToOne
	private Modul modul;
	
	public String getAsbschlussDatum() {
		return asbschlussDatum;
	}

	public void setAsbschlussDatum(String param) {
		this.asbschlussDatum = param;
	}

	public String getKuendigung() {
		return kuendigung;
	}

	public void setKuendigung(String param) {
		this.kuendigung = param;
	}

	public String getBemerkung() {
		return bemerkung;
	}

	public void setBemerkung(String param) {
		this.bemerkung = param;
	}

	public Modul getModul() {
	    return modul;
	}

	public void setModul(Modul param) {
	    this.modul = param;
	}

}