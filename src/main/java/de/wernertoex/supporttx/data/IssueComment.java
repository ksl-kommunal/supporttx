package de.wernertoex.supporttx.data;

import javax.persistence.*;

@Entity
public class IssueComment extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String param) {
		this.comment = param;
	}
}