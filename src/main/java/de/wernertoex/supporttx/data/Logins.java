package de.wernertoex.supporttx.data;

import javax.persistence.Entity;

@Entity
public class Logins extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	private Boolean success;
	private String client;
	public Logins() {
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean param) {
		this.success = param;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String param) {
		this.client = param;
	}
}